import _ from 'lodash';
import './style.css';
// import Background from './construction.jpg';
import printMe from './print.js';

function component() {
  let element = document.createElement('div');
  var btn = document.createElement('button');

  element.innerHTML = _.join(['Hello', 'webpack'], ' ');

  btn.innerHTML = 'Click me and check the console!';
  btn.onclick = printMe;

  element.appendChild(btn);
  // element.classList.add('hello');
  //
  // // Add the image to our existing div.
  // var myBackground = new Image();
  // myBackground.src = Background;
  //
  // element.appendChild(myBackground);

  return element;
}

document.body.appendChild(component());
