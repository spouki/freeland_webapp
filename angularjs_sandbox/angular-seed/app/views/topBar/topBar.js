'use strict'
// class ExampleScene extends Phaser.Scene {
//
//     constructor () {
//         super();
//     }
//
//     preload () {
//         //  It's essential that the key given here is the exact class name used in the JS file. It's case-sensitive.
//         //  See the SceneB.js file and documentation for details.
//         // this.load.sceneFile('ExternalScene', 'assets/loader-tests/ExternalScene.js');
//     }
//
//     create () {
//       console.log('Created ExampleScene');
//         // this.scene.start('myScene');
//
//       var text = this.add.text(80, 550, '', { font: '16px Courier', fill: '#ffffff' });
//
//       text.setText([
//         'Game Title: ' + game.config.gameTitle,
//         'Version: ' + game.config.gameVersion
//       ]);
//     }
//
// }


angular.module('FreeLand.topBar', [])

// angular.module('FreeLand.gamePlayer', ['ngRoute'])
// .config(['$routeProvider', function($routeProvider) {
//   $routeProvider.when('', {
//     templateUrl: 'views/games/game/game.html',
//     controller: 'gameCtrl',
//     controllerAs: 'game'
//   });
// }])
// .controller('gamePlayerCtrl', ['credentialsWatcher', '$compile', '$sce','PhaserLoader', function(credentialsWatcher, $compile, $sce, PhaserLoader) {

.controller('topBarCtrl', ['credentialsWatcher', function(credentialsWatcher) {
  console.log('TopBar');
  if (credentialsWatcher.hasCredentials() == true) {
    // OK
    this.logged = true;
    // this.games = [{'id':0,'name':'Game 1'},{'id':1,'name':'Game 2'}];
    // console.log('Games => ' + JSON.stringify(this.games));
  }
}])
.directive('topBar', function() {
  console.log('dir');
  return {
    restrict: 'E',
    templateUrl:'views/topBar/topBar.html',
    controller:'topBarCtrl',
    controllerAs: 'topBarCtrl'
  };
});
