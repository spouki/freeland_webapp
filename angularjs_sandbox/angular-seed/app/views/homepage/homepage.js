'use strict';

angular.module('FreeLand.homepage', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/homepage', {
    templateUrl: 'views/homepage/homepage.html',
    controller: 'homepageCtrl',
    controllerAs: 'homepage'
  });
}])

.controller('homepageCtrl', ['credentialsWatcher', 'UserAPI', '$timeout', function(credentialsWatcher, UserAPI, $timeout) {
  var that = this;
  this.test = "coucou";
  this.connecting = false;
  this.user = {};

  this.getConnecting = function() {
    // console.log('GetConnecting : ' + this.connecting);
    return this.connecting;
  }


  this.getUser = function() {
    // console.log('USER => ' + this.user.length);
    if (this.user.length === undefined) {
      // console.log('This.getUser FALSE');
      return null;
    } else {
      // console.log('This.getUser TRUE : ' + JSON.stringify(this.user));
      return this.user;
    }
  }


  this.login = {
    "username":"",
    "password":""
  };

  this.isLogged = function() {
    if (credentialsWatcher.hasCredentials() == true) {
      that.connecting = false;
      // console.log('Is logged true');
      return true;
    }
    // console.log('Is logged false');
    return false;
  }



    this.successRetrieveUser = function(data) {
      // console.log('Success retrieve user : ' + JSON.stringify(data));
      that.user = data.data;
    }

    this.errorLogout = function(data) {
      console.log('NOT Succesfully disconnected from service.');
    }

    this.successLogout = function(data) {
      console.log('Succesfully disconnected from service.');
    }

    this.errorRetrieveUser = function(data) {
      console.error('Error retrieve user data : ' + JSON.stringify(data));
      if (data.status == '401') {
        // NEED TO RELOAD ACCESS_TOKEN AND DO THE QUERY AGAIN
        console.error('NEED TO RELOAD ACCESS_TOKEN AND DO THE QUERY AGAIN');
        that.reconnect();
      }
      if (data.data == null) {
        // Service non disponible
        credentialsWatcher.logout(that.successLogout, that.errorLogout);
      }
    }

  this.successAuth = function(data) {
    that.connecting = false;
    // console.log('TEST = ' + this.test);
    // console.log('[SUCCESSAUTH] Data => ' + JSON.stringify(data) + ' GETCONNECTING : ' + that.getConnecting());
    UserAPI.getUserByAccessToken(that.successRetrieveUser, that.errorRetrieveUser);
    that.getUser();
  }

  this.errorAuth = function(data) {
    that.connecting = false;
    // console.log('TEST = ' + this.test);
    // console.error('[ERRORAUTH] Data => ' + JSON.stringify(data) + ' GETCONNECTING : ' + that.getConnecting());
    if (data.data == null) {
      // Service not working
      console.error('Service not reached, either connection or service unavailable.');
    }
  }
  this.auth = function() {
    if (this.login.username == "" || this.login.password == "") {
      // Form empty
      this.connecting = false;
      return false;
    }
    this.connecting = true;
    // $timeout( function(tt) {
    //   // console.log('timeout');
    //   tt.connecting = false;
    // }, 500, true, this );

    console.log('[TRUE] Setting connecting to ' + self.connecting);
    credentialsWatcher.credentialsHandler(this.successAuth, this.errorAuth, this.login);
    return true;
  }

  this.successDisconnect = function(data) {
    // console.log('SuccessDisconnect : ' + JSON.stringify(data));
    that.connecting = false;
  }
  this.errorDisconnect = function(data) {
    console.error('ErrorDisconnect : ' + JSON.stringify(data));
    that.connecting = false;
  }
  this.disconnect = function() {
    // console.log('Disconnecting');
    this.connecting = true;
    // console.log('[TRUE] Setting connecting to ' + this.connecting);
    credentialsWatcher.logout(this.successDisconnect, this.errorDisconnect);
    return true;
  }

  this.successReconnect = function(data) {
    // console.log('Success Reconnect : ' + JSON.stringify(data));
    UserAPI.getUserByAccessToken(this.successRetrieveUser,this.errorRetrieveUser);
  }
  this.errorReconnect = function(data) {
    console.error('Error Reconnect : ' + JSON.stringify(data));
    that.disconnect();
  }

  this.reconnect = function() {
    credentialsWatcher.refreshCredentials(this.successReconnect,this.errorReconnect);
  }

  if (this.isLogged() == true && this.getUser() == null) {
    // console.log('No user profile but with access token');
    UserAPI.getUserByAccessToken(this.successRetrieveUser,this.errorRetrieveUser);
  } else {
    // console.log(' user = ' + JSON.stringify(this.getUser()));
  }
  // console.error('Coucou');

}]);
