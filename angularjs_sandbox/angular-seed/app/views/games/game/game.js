'use strict'
// class ExampleScene extends Phaser.Scene {
//
//     constructor () {
//         super();
//     }
//
//     preload () {
//         //  It's essential that the key given here is the exact class name used in the JS file. It's case-sensitive.
//         //  See the SceneB.js file and documentation for details.
//         // this.load.sceneFile('ExternalScene', 'assets/loader-tests/ExternalScene.js');
//     }
//
//     create () {
//       console.log('Created ExampleScene');
//         // this.scene.start('myScene');
//
//       var text = this.add.text(80, 550, '', { font: '16px Courier', fill: '#ffffff' });
//
//       text.setText([
//         'Game Title: ' + game.config.gameTitle,
//         'Version: ' + game.config.gameVersion
//       ]);
//     }
//
// }


angular.module('FreeLand.gamePlayer', [])

// angular.module('FreeLand.gamePlayer', ['ngRoute'])
// .config(['$routeProvider', function($routeProvider) {
//   $routeProvider.when('', {
//     templateUrl: 'views/games/game/game.html',
//     controller: 'gameCtrl',
//     controllerAs: 'game'
//   });
// }])
// .controller('gamePlayerCtrl', ['credentialsWatcher', '$compile', '$sce','PhaserLoader', function(credentialsWatcher, $compile, $sce, PhaserLoader) {

.controller('gamePlayerCtrl', ['credentialsWatcher', '$compile', '$sce', function(credentialsWatcher, $compile, $sce) {
  console.log('Game');
  this.logged = false;
  this.inited = false;
  this.game = undefined;
  console.log('helo');

  this.main = function() {
    //
    if (this.inited == false) {
      var myCustomCanvas = document.createElement('canvas');

      myCustomCanvas.id = 'myCustomCanvas';
      myCustomCanvas.style = 'border: 8px solid red';

      document.body.appendChild(myCustomCanvas);

      // PhaserLoader.load();
      console.log('First launch');
      // this.bindHtml();
      this.initConfig();

      this.run();
      // this.loadGame();
      // this.displayGameFrame();
    } else {
      console.log('running');
    }
  }


  this.initConfig = function() {
    this.config = {
        type: Phaser.AUTO,
        // parent: 'phaser-container',
        parent: 'game-player',
        width: 800,
        height: 600,
        backgroundColor: '#000',
        scene: {
          preload:this.preload,
          create:this.create,
          // update:this.update
        }
    };
    console.log('Initing : ' + JSON.stringify(this.config));
  }

  this.preload = function() {
    console.log('preload');
  }

  this.create = function() {
    console.log('create');
  }

  this.update = function() {
    console.log('update');
  }

  this.run = function() {
    this.game = new Phaser.Game(this.config);
  }

  this.displayGameFrame = function() {
    // this.game = Phaser();
    // PhaserLoader.getPhaser(this.config);
    console.log('Should display game');
  }

  this.loadGame = function() {
    console.log('Loading game');
  }


  if (credentialsWatcher.hasCredentials() == true) {
    // OK
    this.logged = true;
    this.main();
    // this.games = [{'id':0,'name':'Game 1'},{'id':1,'name':'Game 2'}];
    // console.log('Games => ' + JSON.stringify(this.games));
  }
}])
.directive('gamePlayer', function() {
  console.log('dir');
  return {
    restrict: 'E',
    templateUrl:'views/games/game/game.html',
    controller:'gamePlayerCtrl',
    controllerAs: 'gamePlayerCtrl'
  };
});
