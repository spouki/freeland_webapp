// import ExampleScene from './game/scene.js';
// let Phaser = 'shared/phaser3/phaser.js';
'use strict';


angular.module('FreeLand.games', ['ngRoute'])

// .config(['$routeProvider', function($routeProvider) {
//   $routeProvider.when('/games', {
//     templateUrl: 'views/games/games.html',
//     controller: 'gamesCtrl',
//     controllerAs: 'games'
//   });
// }])

.controller('gamesCtrl', ['credentialsWatcher', '$compile', '$sce', function(credentialsWatcher, $compile, $sce) {
  console.log('Games');
  this.logged = false;


  this.gameArea = function() {//    var html = '<game-player></game-player>';
    // var html = '<game-player></game-player>';
    // var html = '<div class="phaser-example"></div>';
    // var trustedHtml = $sce.trustAsHtml(html);
    // var compiledHtml = $compile(trustedHtml)(this);
    // angular.element(document.getElementById('game-container')).append(compiledHtml);
  }


  this.loadGame = function(id) {
    // document.getElementById('game-player').removeAttribute('hidden');
    // document.getElementById('game-player').setAttribute('hidden',id);
    let game_container = document.getElementById('game-player-container');
    // angular.element(game_container.appendChild(document.createElement('game-player')));



    var html = '<game-player></game-player>';
    var trustedHtml = $sce.trustAsHtml(html);
    var compiledHtml = $compile(trustedHtml)(this);
    var element = document.createElement("game-player");
    element.setAttribute('id','game-player');
    game_container.appendChild(element);
    // .append(compiledHtml);

    console.log('load game');

    // this.gameArea();
    // Create frame for game
    // this.displayGameFrame();
    // Load game data
    // this.game = this.games[id];
    // Run game with preloaded datas
    // console.log('Should load the game data and launch the game when fully loaded');

  }



  if (credentialsWatcher.hasCredentials() == true) {
    // OK
    this.logged = true;
    this.games = [{'id':0,'name':'Game 1'},{'id':1,'name':'Game 2'}];
    console.log('Games => ' + JSON.stringify(this.games));
  }
}])
.directive('gamesList', function() {
  console.log('dir');
  return {
    restrict: 'E',
    templateUrl:'views/games/games.html',
    controller:'gamesCtrl',
    controllerAs: 'gamesCtrl'
  };
});
