'use strict';

angular.module('FreeLand.version', [
  'FreeLand.version.interpolate-filter',
  'FreeLand.version.version-directive'
])

.value('version', '0.1');
