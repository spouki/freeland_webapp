'use strict';

describe('FreeLand.version module', function() {
  beforeEach(module('FreeLand.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
