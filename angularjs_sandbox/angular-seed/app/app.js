'use strict';

// Declare app level module which depends on views, and core components
angular.module('FreeLand', [
  'ngRoute',
  'webStorageModule',
  'oc.lazyLoad',
  'FreeLand.networkManager',
  'FreeLand.credentialsWatcher',
  'FreeLand.UserAPI',
  'FreeLand.GamesAPI',
  'FreeLand.topBar',
  'FreeLand.homepage',
  'FreeLand.games',
  'FreeLand.view2',
  'FreeLand.version',
  // 'FreeLand.PhaserLoader',
  'FreeLand.topBar',
  'FreeLand.gamePlayer'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/homepage'});
}]);
