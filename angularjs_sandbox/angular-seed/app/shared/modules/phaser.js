'use strict';

var app = angular.module('FreeLand.PhaserLoader', [])
    .factory('PhaserLoader', ['$ocLazyLoad', function($ocLazyLoad) {
      var self = this;

      self.load = function() {
        console.log('Loading Phaser');
        this.phaser =  $ocLazyLoad.load('/lib/phaser3/phaser.js');
      }

      self.getPhaser = function() {
        console.log('getPhaser => ' + JSON.stringify(this.phaser));
        return this.phaser;
      }

      self.preload = function() {
        this.load.image('pic','aquarelle.jpg');
      }

      self.create = function() {
        var img = this.add.image(0,0,'pic');

      }



      return self;
      }]
    );
