'use strict';

var app = angular.module('FreeLand.UserAPI', ['webStorageModule'])
    .factory('UserAPI', ['networkManager', 'webStorage', function(networkManager, webStorage) {
      var self = this;
          self.successCall = null;
          self.errorCall = null;

          self.successCallbackMiddle = function(data) {
            self.successCall(data);
          }

          self.errorCallbackMiddle = function(data) {
            self.errorCall(data);
          }

          self.getUserByAccessToken = function(successCallback, errorCallback) {
            self.successCall = successCallback;
            self.errorCall = errorCallback;
            networkManager.getUserByAccessToken(self.successCallbackMiddle, self.errorCallbackMiddle);
          }

          // var that = this;
          return self;
      }]
    );
