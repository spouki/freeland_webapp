'use strict';

var app = angular.module('FreeLand.GamesAPI', ['webStorageModule'])
    .factory('GamesAPI', ['networkManager', 'webStorage', function(networkManager, webStorage) {
      var self = this;
          self.successCall = null;
          self.errorCall = null;

          self.successCallbackMiddle = function(data) {
            self.successCall(data);
          }

          self.errorCallbackMiddle = function(data) {
            self.errorCall(data);
          }

          self.getGames = function(successCallback, errorCallback) {
            self.successCall = successCallback;
            self.errorCall = errorCallback;
            networkManager.getGamesList(self.successCallbackMiddle, self.errorCallbackMiddle);
          }

          self.getGameById = function(id, successCallback, errorCallback) {
            self.successCall = successCallback;
            self.errorCall = errorCallback;
            networkManager.getGamesList(self.successCallbackMiddle, self.errorCallbackMiddle);
          }

          // var that = this;
          return self;
      }]
    );
