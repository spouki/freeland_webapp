'use strict';

var app = angular.module('FreeLand.credentialsWatcher', ['webStorageModule'])
    .factory('credentialsWatcher', ['networkManager', 'webStorage', function(networkManager, webStorage) {
        var self = this;
        self.WATCH_TIME = 3600;

        self.successCall = null;
        self.errorCall = null;

        self.hasCredentials = function() {
          if (webStorage.get('credentials') != null) {
            // console.log('Credentials : ' + JSON.stringify(webStorage.get('credentials')));
            return true;
          }
          return false;
        }

        self.getCredentials = function() {
          return webStorage.get('credentials');
        }

        self.successLogin = function(data) {
          // console.log('SUCCESSLOGIN DATA => ' + JSON.stringify(data.data));
          data = data.data;
          webStorage.set('credentials',data);
            // webStorage.set("access_token", data.access_token);
            // webStorage.set("expire", data.expires_time);
            // webStorage.set("refresh_token", data.refresh_token);
            // webStorage.set("date",data.timestamp);
            // console.log('Access token => (response.data) ' + data.access_token + ' (webstorage)');
            self.successCall(data);
        }

        self.errorLogin = function(data) {
            // console.error("Problem connecting : " + JSON.stringify(data));
            self.errorCall(data);
        }

        self.credentialsHandler = function(successCallback, errorCallback, options = null) {
          if ( ( successCallback === undefined || successCallback === null ) || ( errorCallback === undefined || errorCallback === null ) ) {
            console.error("Error the callback functions are unusable");
            return false;
          }

            if ( webStorage.get("credentials") == undefined ) {
              if (options != null) {
                self.successCall = successCallback;
                self.errorCall = errorCallback;
                networkManager.authSimple(self.successLogin, self.errorLogin, options);
                // console.log('Credentials, relog');
                return true;
              }
              console.error("No credentials");
              return false;
            } else {
              if ( Math.round(+new Date()/1000) -  webStorage.get('credentials')['timestamp'] > self.WATCH_TIME ) {
                self.successCall = successCallback;
                self.errorCall = errorCallback;
                networkManager.authReconnect(self.successLogin, self.errorLogin);
                // console.log('Reconnecting');
              }
            }
            return true;
        }

        self.successLogout = function(data) {
          // console.log("Success disconnected, deleting credentials locally.");
          // Maybe implement a disconnect route on server
          webStorage.remove('credentials');
          self.successCall(data);
        }

        self.errorLogout = function(data) {
          console.error("Error disconnected, deleting credentials locally.");
          // Maybe implement a disconnect route on server
          // webStorage.remove('credentials');
          webStorage.remove('credentials');
          self.errorCall(data);
        }

        self.logout = function(successCallback, errorCallback) {
          if ( ( successCallback === undefined || successCallback === null ) || ( errorCallback === undefined || errorCallback === null ) ) {
            console.error("Error the callback functions are unusable");
            return false;
          }

          self.successCall = successCallback;
          self.errorCall = errorCallback;
          networkManager.logout(self.successLogout, self.errorLogout);

          // Maybe implement a disconnect route on server
          // webStorage.remove('credentials');
          // if (webStorage.get('credentials') != undefined) {
          //   errorCallback({"message":"Can't disconnect"});
          //   // return ();
          // } else {
          //   successCallback({"message":"Disconnected"});
          //   // return ({"message":""});
          // }
        }

        self.successRefreshCallback = function(data) {
          // console.log('Success Refresh Token callback');
          self.successCall(data);
        }

        self.errorRefreshCallback = function(data) {
          console.error('Error Refresh Token callback');
          // No token saved (maybe an older version used)
          self.errorCall(data);
        }

        self.refreshCredentials = function(successCallback, errorCallback) {
          self.successCall = successCallback;
          self.errorCall = errorCallback;
          networkManager.authReconnect(self.successRefreshCallback, self.errorRefreshCallback);
        }

        self.isOutdated = function() {
            // console.log("isOutdated : " , (Math.round(+new Date()/1000) - webStorage.get("date")) , " -> " , (Math.round(+new Date()/1000) - webStorage.get("date") > 3600));
          if (Math.round(+new Date()/1000) - webStorage.get("date") > 3600) {
            return true;
          }
          return false;
        }
        //
        // self.isReady = function() {
        //     if ((webStorage.get("mail") === undefined || webStorage.get("mail") === null) || (webStorage.get("password") === undefined || webStorage.get("password") === null)) {
        //         return false;
        //     }
        //     console.log("Mail : " , webStorage.get("mail") , " Password : " , webStorage.get("password"));
        //     return true;
        // }

        return self;
    }]);
