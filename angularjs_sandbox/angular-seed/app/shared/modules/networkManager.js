
'use strict';

var baseUrl = "http://localhost:3000";

var authBaseUrl = baseUrl + "/auth/";
var userBaseUrl = baseUrl + "/users/";
var adminBaseUrl = baseUrl + "/admin/";
var gamesBaseUrl = baseUrl + "/games/";

var app = angular.module('FreeLand.networkManager', ['webStorageModule'])
    .factory('networkManager', ['$http', 'webStorage', function($http, webStorage) {
        return {
            authSimple: function(successCallback, errorCallback, options) {
                return $http({
                    method:"POST",
                    contentType:"multipart/form-data",
                    url:authBaseUrl + "login",
                    data:options
                }).then(successCallback, errorCallback);
            }

            ,logout: function(successCallback, errorCallback) {
              return $http({
                method:"POST",
                contentType:"multipart/form-data",
                url:authBaseUrl + 'logout'
              }).then(successCallback, errorCallback);
            }

            ,authReconnect: function(successCallback, errorCallback) {
                var authData = {
                    'refresh_token':webStorage.get('credentials')['refresh_token']
                };
                console.log("authData => " + JSON.stringify(authData));
                return $http({
                    method:"POST",
                    contentType:"multipart/form-data",
                    url:authBaseUrl+'refresh',
                    data:authData
                }).then(successCallback, errorCallback);
            }

            ,getUserById: function(id, successCallback, errorCallback) {
              return $http({
                method:'GET',
                url:userBaseUrl+'/'+id,
                headers:{'access_token':webStorage.get('credentials')['access_token']}
              }).then(successCallback, errorCallback);
            }
            ,getUserByAccessToken: function(successCallback, errorCallback) {
              return $http({
                method:'GET',
                url:userBaseUrl+'access_token/access_token',
                headers:{'access_token':webStorage.get('credentials')['access_token']}
              }).then(successCallback, errorCallback);
            }
            ,getGamesList: function(successCallback, errorCallback) {
              return $http({
                method:'GET',
                url: gamesBaseUrl,
                headers:{'access_token':webStorage.get('credentials')['access_token']}
              }).then(successCallback, errorCallback);
            }

        };
    }]);
