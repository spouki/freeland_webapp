class Access_token {
  constructor() {
    this.accesss_token = undefined;
    this.timestamp = undefined;
    this.expire_time = undefined;
    this.refresh_token = undefined;
  }

  set_credentials(new_creds) {
    this.access_token = new_creds.access_token;
    this.timestamp = new_creds.timestamp;
    this.expire_time = new_creds.expire_time;
    this.refresh_token = new_creds.refresh_token;
    return this;
  }

  get_credentials() {
    return this;
  }
};

export default Access_token;
