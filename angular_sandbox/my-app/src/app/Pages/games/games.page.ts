import { Component, OnInit } from '@angular/core';
import {AccessTokenService} from '../../Shared/Services/AccessToken/access-token.service';
import {ApiService} from '../../Shared/Services/API/api.service';
import {Router} from "@angular/router"

@Component({
  templateUrl: './games.page.html',
  styleUrls: ['./games.page.css']
})
export class GamesPage implements OnInit {

  selected:boolean;

  constructor(private api: ApiService, private accesstoken: AccessTokenService,private router: Router) {
    this.selected = false;
      if (this.api == null) {
        console.error('API empty');
          // this.router.navigate(['appareils']);
      }
      if (!this.accesstoken.is_logged()) {
        console.error('Not logged in, should try reconnect or go index');
        if (!this.accesstoken.has_credentials()) {
          console.log('Has no credentials');
          this.router.navigate(['/']);
        } else {
          console.log('Might have some logs, try reconnect');
          this.api.refresh_token().subscribe(resp=>{
            console.log('REfreshed token : ' + JSON.stringify(resp));
          });
        }
      }

  }

  ngOnInit() {
  }

  loadGame(id_game) {
    console.log('Loaded game ' + id_game);
    this.selected = true;
  }

}
