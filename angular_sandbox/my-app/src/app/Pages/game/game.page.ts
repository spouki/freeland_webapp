import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
// import {SceneService} from './scene/service';
import {PhaserComponent} from '../../Shared/Components/phaser/phaser.component';

@Component({
  selector:'game-page',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.css'],
  // providers: [SceneService]
})
// export class GamePage {
export class GamePage  {

  private phaser_c : PhaserComponent;

  constructor() {
    this.phaser_c = new PhaserComponent();
  }

  runGame() {
    console.log('Runned');
    this.phaser_c.run();
  }

  bootGame() {
    console.log('Booted');
    this.phaser_c.boot();
    this.runGame();
  }
}
