import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// let bootpath = '/app/';
// let services = bootpath+'/app/Shared/Services/';
import {AccessTokenService} from '../../Shared/Services/AccessToken/access-token.service';
import {ApiService} from '../../Shared/Services/API/api.service';

@Component({
  // selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.css']
  // providers:[AccessTokenService, BackendApiService]
})
export class HomePage implements OnInit {

  // public logged: boolean;

  constructor(private router: Router, private route: ActivatedRoute, private accesstoken: AccessTokenService, private api: ApiService) {
    // if ()
    // if (this.accesstoken.isLogged()) {
    //   this.logged = this.isLogged();
    // } else {
    //   this.logged = this.isLogged();
    // }
  }

  logout() {
    this.api.logout().subscribe(resp => {
      console.log('Response ? ' + JSON.stringify(resp.body));
      // Should define code to know what to do OR get routes by api
      this.accesstoken.clear();
      // this.logged = this.isLogged();
    });
  }


  ngOnInit() {
    // this.logIt('OnInit');
  }

  logIt(msg: string) {
    console.log(`#${msg}`);
  }

  isLogged() {
    // console.log('isLogged = ' + this.accesstoken.isLogged());
    // console.log('ACCESS TOKEN = ' + JSON.stringify(this.accesstoken.getAccessToken()));
    return this.accesstoken.is_logged();
  }

}
