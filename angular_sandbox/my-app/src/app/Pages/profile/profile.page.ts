import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AccessTokenService} from '../../Shared/Services/AccessToken/access-token.service';
import {ApiService} from '../../Shared/Services/API/api.service';
import {UserService} from '../../Shared/Services/UserService/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})
export class ProfilePage implements OnInit {
  mount_id: number;
  headers: string[];

  constructor(private router: Router, private route: ActivatedRoute,
    private api: ApiService, private accesstoken: AccessTokenService,
    private user: UserService) {
      console.log('Chimera');
      if (this.api == null) {
        console.error('API empty');
          // this.router.navigate(['appareils']);
      }

    // if (this.user.getUser() == null) {
    //   console.log('Shit');
    //   this.api.getUserByToken().subscribe(resp => {
    //     console.log('Response ? ' + JSON.stringify(resp.body));
    //     this.accesstoken.ping();
    //     this.accesstoken.save(resp.body);
    //   });
    //   this.user.refreshUser();
    // } else {
    //
    // }
  }

  ngOnInit() {
    this.mount_id = this.route.snapshot.params['id'];
    if (this.mount_id == undefined) {
      // self profile
      console.log('Getting self user');
      this.api.get_user_by_token().subscribe((resp)=>{
        console.log('Got response from backend for getting user by access token : ' + JSON.stringify(resp));
        const keys = resp.headers.keys();
        this.headers = keys.map(key =>
          `${key}: ${resp.headers.get(key)}`
        );
      });
    } else {
      console.log('Profile id = ' + this.mount_id);
    }
  }

}
