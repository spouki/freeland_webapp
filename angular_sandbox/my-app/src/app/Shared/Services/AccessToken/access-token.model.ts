import { Injectable} from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class AccessTokenModel {
  access_token : string;
  refresh_token : string;
  expire_time : number;
  timestamp : number;
}
