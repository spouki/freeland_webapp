import { Inject,Injectable } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
// import {ApiService} from '../../Services/API/api.service';

const STORAGE_KEY = 'access_token';

@Injectable({
  providedIn: 'root'
})
export class AccessTokenService {
  constructor(@Inject(SESSION_STORAGE) private storage: StorageService) {
  }

  ping() {
    this.log('PING');
    return true;
  }
  log(message: string) {
    console.log(`AccessTokenService: ${message}`);
  }

  get_access_token() {

    return this.storage.get(STORAGE_KEY)['access_token'];
  }

  get_refresh_token() {
    return this.storage.get(STORAGE_KEY)['refresh_token'];
  }

  is_logged() {
    let data = this.storage.get(STORAGE_KEY);
    if (data == undefined) {
      return false;
    }
    if (data.access_token != null) {
      if (Number(data.timestamp) + Number(data.expire_time) > Number(+Date.now()/1000 + 50)) {
        // console.log('Is logged');
        return true;
      }
    }
    return false;
  }

  save(new_access_token) {
    // this.data = new_access_token;
    this.storage.set(STORAGE_KEY, new_access_token);
  }

  clear() {
    this.storage.set(STORAGE_KEY,null);
  }

  has_credentials() {
    return this.get_access_token() ? true : (this.get_refresh_token() ? true: false);
  }

}
