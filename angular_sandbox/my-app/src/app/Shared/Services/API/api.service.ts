import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { AccessTokenService } from './AccessToken/access-token-service.service';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
// import { AccessTokenModel } from './AccessToken/access-token.model';
import {AccessTokenService} from '../AccessToken/access-token.service';

@Injectable({
  providedIn: 'root'
})


export class ApiService {

  private loggedHttpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'access_token':undefined,
    })
  };

  constructor(private http: HttpClient, private accesstoken: AccessTokenService) {
    this.log('In ApiService');
  }
  // logged in area

//AUTH
  logout() {
    // if (this.loggedHttpOptions.headers['access_token'] == undefined) {
    //   return ;
    // }
    if (this.accesstoken.is_logged()) {
      console.log('Logged in');
    }
    this.loggedHttpOptions.headers['access_token'] = this.accesstoken.get_access_token();

    let url = "http://localhost:3000/auth/logout";
    return this.http.post(url, {}, {observe:'response'});
  }
  refresh_token() {
    // if (this.loggedHttpOptions.headers['access_token'] == undefined) {
    //   return null;
    // }
    if (this.accesstoken.is_logged()) {
      console.log('Logged in');
    }
    this.loggedHttpOptions.headers['access_token'] = this.accesstoken.get_access_token();
    let url = "http://localhost:3000/auth/refresh";
    return this.http.post(url,{},{observe:'response'});
  }
//


  //USERS
  get_user_by_token() {
    console.log('Getting user by access token');
    if (this.accesstoken.is_logged()) {
      console.log('Logged in');
    }
    this.loggedHttpOptions.headers['access_token'] = this.accesstoken.get_access_token();
    console.log('Access token : ' + this.loggedHttpOptions.headers['access_token']);
    let url = "http://localhost:3000/users/access_token/";
    return this.http.get(url,{observe:'response'});
  }
  private log(message: string) {
    console.log(`BackendApiServiceService: ${message}`);
  }
  //

  // free area

  //AUTH
  login(form)  {
    let url = "http://localhost:3000/auth/login";
    this.log('In login function');
    return this.http.post(url,form,{observe:'response'});
  }
  //
}
