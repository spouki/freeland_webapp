import { ModuleWithProviders,Inject,Injectable, Optional } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import {ApiService} from '../../Services/API/api.service';

const STORAGE_KEY = 'user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(@Inject(SESSION_STORAGE) private storage: StorageService, private api: ApiService) {
    // if (this.getUser() == null) {
    //   this.refreshUser();
    // }
  }

  ping() {
    this.log('PING');
    return true;
  }
  log(message: string) {
    console.log(`AccessTokenService: ${message}`);
  }

  get_user() {
    let user = this.storage.get(STORAGE_KEY);
    if (user == null) {
      console.error('Cant get user, nothing in storage');
      this.refresh_user().subscribe(resp => {
        console.log('Refreshed user data : ' + JSON.stringify(resp.body));
        this.save(resp.body);
        return resp.body;
      });

    } else {
      return user;
    }
  }


  refresh_user() {
    // this.api.
    console.log('TODO REFRESH USER BY API');
    return this.api.get_user_by_token();
  }

  save(user_data) {
    // this.data = new_access_token;
    this.storage.set(STORAGE_KEY, user_data);
  }

  clear() {
    this.storage.set(STORAGE_KEY,null);
  }

}
