import { Component, OnInit } from '@angular/core';
import {AccessTokenService} from '../../Services/AccessToken/access-token.service';
import {ApiService} from '../../Services/API/api.service';

@Component({
  selector: 'menu-component',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  // private logged: boolean;

  constructor(private accesstoken: AccessTokenService, private api: ApiService) {
    // this.logged = this.accesstoken.isLogged();
    // if (this.logged == true) {
    //   console.log('(MenuComponent) : Access token exists : ' + JSON.stringify(this.accesstoken.getAccessToken()));
    // }
  }

  is_logged() {
    // console.log('Is logged ? ' + this.accesstoken.is_logged());
    return this.accesstoken.is_logged();
  }

  ngOnInit() {
  }

}
