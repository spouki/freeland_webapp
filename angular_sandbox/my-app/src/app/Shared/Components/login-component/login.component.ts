import { Component, OnInit } from '@angular/core';
import { Directive, ViewContainerRef } from '@angular/core';
import {login_component_class} from './login.component.class';
import {AccessTokenService} from '../../Services/AccessToken/access-token.service';
import {ApiService} from '../../Services/API/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
  // providers:[AccessTokenService]
})

export class LoginComponent implements OnInit {

  login_form: login_component_class;

  constructor(private router: Router, private api: ApiService, private accesstoken: AccessTokenService) {
    if (this.accesstoken.is_logged() == true) {
      console.log('(LoginComponent) : Access token already exists : ' + JSON.stringify(this.accesstoken.get_access_token()));
    }
    console.log('Login component');
    this.login_form = new login_component_class();
    this.login_form.username = "";
    this.login_form.password = "";
  }

  ngOnInit() {
    console.log('OnInit');
  }

  login() {
    console.log('Login : ' + this.login_form.username);
    console.log('Form : ' + JSON.stringify(this.login_form));
    this.api.login(this.login_form).subscribe(resp => {
      console.log('Response ? ' + JSON.stringify(resp.body));
      this.accesstoken.ping();
      this.accesstoken.save(resp.body);
    });
  }

}
