import { Component,OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import {SceneService} from './scene/service';

@Component({
  selector:'phaser-component',
  templateUrl: './phaser.view.html',
  providers: [SceneService]
})

export class PhaserComponent implements OnInit, OnDestroy, AfterViewInit {

    game: Phaser.Game;

    public readonly gameConfig: GameConfig = {
      type: Phaser.AUTO,
      width: 800,
      height: 600,
      physics: {
        default: 'arcade',
        arcade: {
          gravity: { y: 200 },
          debug: false,
        }
      },
      parent: 'game-container',
    };

    boot():void {
      this.game = new Phaser.Game(this.gameConfig);
    }

    run():void {
      if (this.game != undefined) {
        this.game.events.once('ready', () => {
          this.game.scene.add('Scene', new SceneService(), true);
        });
      } else {
        console.error('Game not defined');
      }
    }

    ngOnInit(): void {
      this.game = new Phaser.Game(this.gameConfig);
    }

    ngOnDestroy() {
      if (this.game != undefined) {
        this.game.destroy(true);
      }
    }

    ngAfterViewInit() {
      if (this.game != undefined) {
        this.game.events.once('ready', () => {
          this.game.scene.add('Scene', new SceneService(), true);
        });
      }
    }
}
