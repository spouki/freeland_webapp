import { NgModule } from '@angular/core';

// import {Phaser} from 'phaser';
import { PhaserComponent } from './phaser.component';
// import * as Phaser from 'phaser';

/**
 * Phaser module. Exports [PhaserComponent]{@link PhaserComponent}.
 */
@NgModule({
  declarations: [PhaserComponent],
  exports: [PhaserComponent]
})
export class PhaserModule { }
