import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { FormsModule } from '@angular/forms'; // Important for binding forms to controller
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Route } from '@angular/router';


// static const shared = '/Shared/';
// static const services = shared+'Services/';
//
// import {PathsService} from './Services/PathsService';
// let bootpath = new PathsService();
// Services
// let bootpath = '/app/';
// let services = bootpath+'Shared/Services/';
// AccessTokenService
import {AccessTokenService} from './Shared/Services/AccessToken/access-token.service';
// import {AccessTokenService} from services+'AccessToken/access-token.service';

// BackendApiService
import {ApiService} from './Shared/Services/API/api.service';
// import {BackendApiService} from services+'backend-api.service';
// UserService
import {UserService} from './Shared/Services/UserService/user.service';
//

import { PhaserModule } from './Shared/Components/phaser/phaser.module';


// let components = shared+'Components/';
//
// Components
// let components = bootpath+'Shared/Components/');
//
import { LoginComponent } from './Shared/Components/login-component/login.component';
// import { LoginComponent } from components+'login-component/login.component';
import { MenuComponent } from './Shared/Components/menu/menu.component';
// import { MenuComponent } from components+'menu/menu.component';

// APP COMPONENT
import { AppComponent } from './app.component';
//


// let pages = '/Pages/';
// let homepage = pages+'home/';
// let page_not_found = pages+'page-not-found/';
// let profilepage = pages+'profile/';
//
// Pages
// let pages = bootpath+'Pages/';
//
import { HomePage } from './Pages/home/home.page';
import { ProfilePage } from './Pages/profile/profile.page';
import { PageNotFoundPage } from './Pages/page-not-found/page-not-found.page';
import { GamesPage } from './Pages/games/games.page';
import { GamePage } from './Pages/game/game.page';
//
//

// let ActiveXObject = new ActiveXObject("GameX");
// import * as Phaser from 'phaser';


//https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular/5088826-gerez-la-navigation-avec-le-routing#/id/r-5089064

const ROUTES: Route[] = [
  { path: '', component: HomePage },
  { path: 'home', redirectTo: '', pathMatch: 'full' },
  { path: 'profile', component: ProfilePage},
  { path: 'profile/:id', component: ProfilePage},
  { path: 'games', component: GamesPage},
  // { path: 'messages', component: MessagesComponent},
  // { path:'', component: },
  // { path: 'redirectMe', redirectTo: 'home', pathMatch: 'full' },
  // { path: 'users/:userid', component: UserComponent,
  //   children: [
  //     { path: 'notes', component: NotesComponent },
  //     { path: 'notes/:noteid', component: NoteComponent}
  //   ]
  // },
  // { path: 'secondary1', outlet: 'sidebar', component: Secondary1Component },
  // { path: 'secondary2', outlet: 'sidebar', component: Secondary2Component },
  { path: '**', component: PageNotFoundPage },
];


@NgModule({
  imports: [
    BrowserModule,
    FormsModule, // Important for binding forms to controller
    HttpClientModule,
    StorageServiceModule,
    PhaserModule,
    RouterModule.forRoot(ROUTES)
  ],
  declarations: [
    AppComponent,
    MenuComponent,
    LoginComponent,
    HomePage,
    PageNotFoundPage,
    ProfilePage,
    GamesPage,
    GamePage
  ],
  providers: [AccessTokenService, ApiService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule {

}
