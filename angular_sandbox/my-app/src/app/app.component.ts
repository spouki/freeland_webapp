import { Component } from '@angular/core';
// import {BackendApiService} from './Shared/Services/BackendApiService/backend-api.service';
// import {AccessTokenService} from './Shared/Services/AccessToken/access-token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';

  constructor() {}
  // Check user session
  // constructor(private api: BackendApiService, private accesstoken: AccessTokenService) {
    // if (this.accesstoken.isLogged())
    // this.isUnchanged = true;
  // }
}
